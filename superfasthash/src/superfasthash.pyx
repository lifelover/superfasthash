#cython: language_level=3

cdef unsigned int _get_16_bits(const unsigned char[:] ptr):
    return ptr[0] + (ptr[1] << 8)

def hash(const unsigned char[:] data):
    """
    Stream-adapted SuperFastHash algorithm from Paul Hsieh,
    http://www.azillionmonkeys.com/qed/hash.html
    LGPLv2.1
    Python version with no dependencies.
    Victor Perron <victor@iso3103.net>

    Cython version with no dependencies.
    Juan Méndez <vpsink@gmail.com>
    """
    cdef unsigned int hash =  0

    if data is None:
        return hash

    cdef unsigned int len_ = len(data)
    cdef unsigned int rem = len_ & 3
    len_ >>= 2


    # Main loop
    while len_ > 0:
        len_ -= 1
        hash += _get_16_bits(data) 
        tmp   = (_get_16_bits(data[2:]) << 11) ^ hash
        hash  = (hash << 16) ^ tmp
        data  = data[4:]
        hash += (hash >> 11)

    # Handle end cases
    if rem == 3:
        hash += _get_16_bits (data)
        hash ^= (hash << 16) 
        hash ^= (data[2] << 18) 
        hash += (hash >> 11)
    elif rem == 2:
        hash += _get_16_bits (data)
        hash ^= (hash << 11) 
        hash += (hash >> 17)
    elif rem == 1:
        hash += data[0]
        hash ^= (hash << 10) 
        hash += (hash >> 1)

    # Force "avalanching" of final 127 bits
    hash ^= (hash << 3) 
    hash += (hash >> 5)
    hash ^= (hash << 4) 
    hash += (hash >> 17)
    hash ^= (hash << 25) 
    hash += (hash >> 6)
    
    return hash