# superfasthash
 
**Cython** port of vperron's [python-superfasthash](https://github.com/vperron/python-superfasthash) which is originally based on Paul Hsieh's [SuperFastHash algorithm](http://www.azillionmonkeys.com/qed/hash.html).

* Fast as hell: Way faster than the original pure-python implementation.
* Coherent: This port only allows hashing of bytes/bytearray objects, unlike the old version which literally uses strings and unnecessary things with `ord`. (wtf?)

* Dependency-agnostic: no python dependency whatsoever
* As minimal as possible.

This is just a 1:1 cython port; there are no tests. Push requests are welcomed.

## How to install/use

Cython is required to build this library. You can install it with either pip inside a venv (recommended) or with your system's package manager.

```bash
git clone https://gitlab.com/lifelover/superfasthash.git
cd superfasthash/
mkdir superfasthash/
python setup.py build_ext
pip install -e .
cd ../ && rm -rf superfasthash/
```

This will clone the repo, build the cython extension and then delete the (now) unused folder. You can the use it like this: 

```python
import superfasthash
superfasthash.hash(b'i use arch linux btw')
>>> 1313238879
```


## License

The license for this code is [LGPL 2.1](http://www.gnu.org/licenses/lgpl-2.1.txt).
It matches exactly the one chosen by Paul Hsieh, whose original statement is reproduced below.

```
IMPORTANT NOTE: Since there has been a lot of interest for the code below, I have decided to additionally provide it under the LGPL 2.1 license. This provision applies to the code below only and not to any other code including other source archives or listings from this site unless otherwise specified.

The LGPL 2.1 is not necessarily a more liberal license than my derivative license, but this additional licensing makes the code available to more developers. Note that this does not give you multi-licensing rights. You can only use the code under one of the licenses at a time. 
```
