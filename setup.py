from setuptools import setup
from Cython.Build import cythonize

setup(
	name = 'superfasthash',
	packages = ['superfasthash'],
	version = '0.1',
	license='LGPL',
	description = 'Cython port of SuperHash',
	author = 'JUAN MENDEZ',
	author_email = 'vpsink@gmail.com',
	url = 'https://gitlab.com/lifelover/superfasthash',
	download_url = 'https://gitlab.com/lifelover/superfasthash/-/archive/master/superfasthash-master.tar.gz',
	keywords = ['superfasthash', 'cython', 'python'],
	install_requires=[],
	ext_modules = cythonize("superfasthash/src/superfasthash.pyx")
)
